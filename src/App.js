import './App.css';
import Expenses from './components/Expenses';
import NewExpense from './components/NewExpense/NewExpense';

function App() {
  const expenses = [
    { title: 'Car Insurance', amount: 294.67, date: new Date(2021, 2, 28)},
    { title: 'Car Insurance', amount: 294.67, date: new Date(2021, 2, 28)},
    { title: 'Car Insurance', amount: 294.67, date: new Date(2021, 2, 28)},
    { title: 'Car Insurance', amount: 294.67, date: new Date(2021, 2, 28)},
  ];

  return (
    <div className="App">
      <h1>Let's get started.</h1>
      <NewExpense />
      <Expenses items={expenses} />
    </div>
  );
}

export default App;
